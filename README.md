# NAME

Astro::FITS::CFITSIO::Utils - FITS utility routines

# VERSION

version 0.15

# SYNOPSIS

    use Astro::FITS::CFITSIO::Utils;

# DESCRIPTION

This is a bundle of useful FITS routines which use CFITSIO.

## Errors

Errors are generally returned by **croak()**ing.  Error messages
will begin with `Astro::FITS::CFITSIO::Utils`.

# INTERNALS

# Functions

- keyval

    This is a wrapper around **keypar** which sets the `Value` option.
    For example, instead of this kludge:

        $value = keypar( $file, $keyword, { Value => 1 } );

    you can type

        $value = keyval( $file, $keyword );

    Everything else is the same as **keypar** (including the error messages,
    which refer to **keypar**).

- keypar

        # single keyword, return first match
        $myitem  = keypar( $file, $keyword, [\%opts] );

        # single keyword, multiple HDU matches
        @myitems = keypar( $file, $keyword, [\%opts] );


        # multiple keywords
        @items = keypar( $file, \@keyw, [\%opts] );

    This routine searches the headers in the specified FITS file for a
    keyword with the given name.  `$file` may be a filename or a file heandle
    returned by **Astro::FITS::CFITSIO::open\_file()**.  In the latter case
    the handle's current HDU is restored after the call to **keypar**.

    The matching keywords are returned either as
    **Astro::FITS::CFITSIO::Utils::Item** objects which inherit from
    **Astro::FITS::Header::Item** objects, as the value of the keyword (if
    the `Value` option is specified), or **undef** if no match was found.
    The **myItem** object adds a member **hdu\_num** which records the number
    of the HDU in which the keyword is found.

    A single keyword may be matched multiple times in an HDU (if it is
    either `COMMENT` or `HEADER`) as well as in multiple HDU's.
    This behavior is regulated with the `Accumulate` and `OnePerHDU`
    option flags, which are passed via the optional hashref argument (`\%opts`).

    If `Accumulate` is set, all of the HDU's are scanned for the
    keyword(s).  If `OnePerHDU` is set, only the first match in an HDU is
    returned.

    The default values for these options depends upon the context in
    which **keypar** is called. **keypar** attempts to provide the most
    intuitive behavior.

    - If a single keyword is passed (as a scalar) and **keypar** is called in
    a scalar context, the first match is returned.
    - If a single keyword is passed (as a scalar) and **keypar** is called in
    a list context, `Accumulate` defaults to 1 and `OnePerHDU` to 0.
    This means that all possible matches will be returned.  Recall that
    `OnePerHDU` only affects `COMMENT` and `HEADER` keywords.
    - If an arrayref of keyword(s) is passed `Accumulate` defaults to 0 and
    `OnePerHDU` to 1.  This results in the following behavior:

            ( $hdr_keyw1, $hdr_keyw2 ) =
                    keypar( $file, [ $keyw1, $key2 ] );

        If either `OnePerHDU` = 0 or `Accumulate` = 1, a keyword might
        match multiple times, and the returned values are arrayrefs containing
        the list of matched items:

            ( $arrayref_keyw1, $arrayref_keyw2 ) =
                   keypar( $file, [ $keyw, $keyw2],
                            { Accumulate => 1 } );

    The available option flags are:

    - Accumulate

        If set, matching keywords from all HDU's are returned, not just from
        the first HDU which has one.  This defaults to `0` (off).

    - CurrentHDU

        Scan only the current HDU.  This defaults to `1` (on) if the filename
        contains extension information, `0` (off) otherwise.  **keypar** uses
        a crude method of determining if extension information is present (it
        checks for the `[` character in the filename ), so it may be confused
        if a filter expression is part of the filename.

    - OnePerHDU

        If set, only one match will be made per HDU. This affects only the
        `COMMENT` and `HEADER` keywords.

    - Value

        If non-zero, returns the _value_ of the keyword, rather than a reference
        to the keyword object.

- colkeys

        %colkey = colkeys( $filename, [\%opts] );

    Retrieve the keywords associated with columns in a table in the given
    FITS file.  If no HDU is specified in the options, it will use
    the first table HDU found.

    The following options are recognized:

    - extname

        The name of the extension from which to retrieve the keywords.

    - extver

        The version of the extension from which to retrieve the keywords.
        Ignored if `extname` is not also specified.  If not specified,
        the first extension with a matchine `extname` is used.

    The keys in the returned hash are the lowercased column names.  The
    values are hashrefs, with the following values:

    - idx

        The unary based index of the column in the extension

    - hdr

        A hashref.  The hash keys are the lower-cased names of the keywords
        for the given column, with the trailing column index removed.  The
        hash values are the keyword values.

- croak\_status

            croak_status($status, @msg );

    **Deprecated**: use the **Astro::FITS::CFITSIO::CheckStatus** module
    instead.

    Checks the CFITSIO status variable. If it indicates an error,
    it **croak()**'s, outputting first the passed message, then the
    the corresponding CFITSIO error message. A newline character `\n`
    will be appended to the message.

    The carp level is adjusted to make the croak appear to be called from
    the calling routine.

# SUPPORT

## Bugs

Please report any bugs or feature requests to bug-astro-fits-cfitsio-utils@rt.cpan.org  or through the web interface at: https://rt.cpan.org/Public/Dist/Display.html?Name=Astro-FITS-CFITSIO-Utils

## Source

Source is available at

    https://gitlab.com/djerius/astro-fits-cfitsio-utils

and may be cloned from

    https://gitlab.com/djerius/astro-fits-cfitsio-utils.git

# AUTHOR

Diab Jerius <djerius@cpan.org>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2023 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007
